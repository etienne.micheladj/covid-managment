import gym
from gym.utils import seeding
import numpy as np

class Simple_v0 (gym.Env):
    MAX_STEPS = 200

    dict_actions = {
        0:(0,0),
        1:(-0.04,0.03),
        2:(-0.03,0.07),
        3:(-0.08,0.010),
        4:(-0.01,0.02),
        5:(0.04,0),
        6:(0.03,0),
        7:(0.08,0)
    }


    def __init__(self):
        self.action_space = gym.spaces.Discrete(7)
        self.observation_space = gym.spaces.Box(np.array([0,0]), np.array([1,1]))
        self.coeffs_env_health = [0 for _ in range(self.MAX_STEPS)]
        event_period_start = 15
        event_period_end = 100
        coeff_crisis = -0.2
        self.coeffs_env_health[event_period_start:event_period_end] = [coeff_crisis for _ in range(event_period_end - event_period_start)]
        
        self.ecos = []
        self.healths = []
        
        self.init_position = np.array([0.5,0.5])
        self.t_eco = 0.1
        self.t_health = 0.1

        self.seed()
        self.reset()

    def reset(self):
        #self.position = 
        self.count = 0
        self.state = self.init_position
        self.reward = 0
        self.done = False
        self.info = {}
        self.rewards = []

        return self.state

    def step(self, action):
        self.eco = self.state[0]
        self.health = self.state[1]
        if self.done:
            print("EPISODE DONE !!!")
        elif self.count == self.MAX_STEPS:
            self.done = True
        elif self.eco <= self.t_eco or self.health <= self.t_health:
            self.done=True
        else:
            assert self.action_space.contains(action)
            self.count +=1

            # insert simulation logic to handle an action ...
            action_points = self.dict_actions[action]
            health_points = action_points[1]
            eco_points = action_points[0]
            coeff_env_health = self.coeffs_env_health[self.count -1]
            coeff_health = coeff_env_health + health_points
            coeff_eco = eco_points

            self.previous_step = (self.health + self.eco)/2
            self.health += coeff_health
            self.eco += coeff_eco

            if self.health >= 1:
                self.health = 1
            if self.eco >= 1:
                self.eco = 1


            self.current_step = (self.health + self.eco)/2
            self.reward = 1 - (self.current_step/self.previous_step)
            self.state = np.array([self.eco,self.health])
            self.save_values()
            self.rewards.append(self.reward)
            self.info["rewards"] = self.rewards


        try:
            assert self.observation_space.contains(self.state)
        except AssertionError:
            #print("INVALID STATE : ", self.state)
            pass
        
        return [self.state, self.reward, self.done, self.info]


    def _render_to_file(self, filename='render.txt'):
        
        file = open(filename, 'a+')
        file.write(f'Eco: {self.state[0]}\n')
        file.write(f'Health: {self.state[1]}\n')
        file.close()

    def save_values(self):
        self.ecos.append(self.state[0])
        self.healths.append(self.state[1])


    def render(self):
        print(self.state)
        self._render_to_file()


    def seed (self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def close (self):
        pass