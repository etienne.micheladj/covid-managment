import gym
import gym_env
from simple_env import Simple_v0
import matplotlib.pyplot as plt

def run_one_episode(env):
    env.reset()
    sum_reward = 0

    for i in range(env.MAX_STEPS):
        action = env.action_space.sample()
        state, reward, done, info = env.step(action)
        env.render()
        if done:
            break

    plt.figure()
    plt.plot(env.ecos, label="economie")
    plt.plot(env.healths, label="santé")
    plt.legend()
    plt.ylim(-0.01,1.01)
    plt.show()
    return sum_reward

if __name__ == "__main__":
    env = Simple_v0()
    sum_reward = run_one_episode(env)